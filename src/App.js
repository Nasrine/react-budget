import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home } from './Pages/Home';
import ButtonAppBar from './Navbar/Navbar';

function App() {
  return (
    
    
      
    <div>
      
      <BrowserRouter>
      <ButtonAppBar />
        <Switch>
          <Route exact path='/'>
            <Home/>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;

import { useState } from "react";



const initialState = {
    montant:"",
    categorie:"",
     date: '',   
}
// pour exporter une function dans un autre fichier 
export function FormOperation({ onFormSubmit }) {
//une variable qui s'apelle Hook pour faire un changement 
    const [form, setForm] = useState(initialState);
    //setForm c'est lui qui prend le controle de la variable qui s'apelle form
    

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);
    }

    return (
        <form onSubmit={handleSubmit}>
 <label>montant:</label>
            <input className="form-control" required type="number" name="montant" onChange={handleChange} value={form.montant}/>
            <label>Categorie:</label>
            <input className="form-control" required type="text" name="categorie" onChange={handleChange} value={form.categorie}/>
            <label>Date:</label>
            <input className="form-control" required type="date" name="date" onChange={handleChange} value={form.date}/>
            <button className="btn btn-primary">Submit</button>
        </form>
    );
}

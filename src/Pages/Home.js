import axios from "axios";
import { useEffect, useState } from "react"
import { DropDown } from "../Components/DropDown";
import { FormOperation } from "../Components/FormOperation";
import { Operation } from "../Components/Operation";




export function Home() {

    const [operations, setOperation] = useState([]);

    async function fetchBudget() {
        const response = await axios.get('https://nasrinebudget.herokuapp.com/api/budget/all');

        setOperation(response.data);
    }

    async function deleteOperation(id) {
        await axios.delete('https://nasrinebudget.herokuapp.com/api/budget/' + id);
        setOperation(
            operations.filter(item => item.id !== id)
        );
    }

    async function addOperation(operation) {
      await axios.post ('https://nasrinebudget.herokuapp.com/api/budget', operation)

     }



    useEffect(() =>{
        fetchBudget();
    }, []);

    function Price(){
        let total = 0;
        for (const element of operations) {
            total += element.montant
    
    
        }return total
    }
    async function fetchMonth(month){
        const response = await axios.get("https://nasrinebudget.herokuapp.com/api/budget/month/" +month)
        setOperation(response.data)
    }
    
    return (
        
        <div className="container">
<div >


            <h1 className="titre">Welcome to my budget</h1> 
            
            <div className="total">Total :<span>{Number.parseFloat(Price()).toFixed(2)}€</span></div> </div>
            <DropDown reset={fetchBudget} selectMonth={fetchMonth} />

            <table className="table table-striped align-items-center">
               
                
  <thead>
    <tr>
      <th >montant</th>
      <th >categorie</th>
      <th >date</th>
      <th >delete</th>
    </tr>
  </thead>
  <tbody>
      
            
                {operations.map(item =>
                    <Operation key={item.id}
                        operation={item}
                        onDelete={deleteOperation}
                        />
                )}

</tbody>
</table>


<section className="row">
                <FormOperation onFormSubmit={addOperation}/>
                          </section>
            
        </div>
    )
}

